package bkap.labguides.viewdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.MediaController;

import java.io.File;
import java.io.IOException;

import bkap.labguides.R;

public class AudioDemoActivity extends AppCompatActivity {
    final String TAG="AudioDemoActivity";

    MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio_demo);

        mediaPlayer = new MediaPlayer();

        try {
            AssetFileDescriptor assetFileDescriptor = getAssets().openFd("snd_bg.mp3");
            mediaPlayer.setDataSource(assetFileDescriptor);
            mediaPlayer.prepare();
            mediaPlayer.start();

        } catch (IOException e) {
            e.printStackTrace();
        }catch (Exception e){
            Log.d(TAG, "onCreate: ");
            e.printStackTrace();
        }

    }
}
