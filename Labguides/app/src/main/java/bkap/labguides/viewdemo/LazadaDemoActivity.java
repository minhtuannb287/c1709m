package bkap.labguides.viewdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;

import bkap.labguides.R;

public class LazadaDemoActivity extends AppCompatActivity {

    Toolbar myToolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lazada_demo);

        myToolbar = findViewById(R.id.toolbar_lazada);
        setSupportActionBar(myToolbar);
    }
}
