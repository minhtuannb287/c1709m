package bkap.labguides.viewdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import bkap.labguides.R;

public class TableLayoutDemoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table_layout_demo);
    }
}
