package bkap.labguides.viewdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import bkap.labguides.R;
import bkap.labguides.adapters.CountryApdater;
import bkap.labguides.models.Country;

public class ListViewDemoActivity extends AppCompatActivity {

    List<Country> countryList;
    CountryApdater countryApdater;
    ListView lv_country;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view_demo);

        initData();

        countryApdater = new CountryApdater(this, R.layout.item_country, countryList);
        lv_country = findViewById(R.id.lv_country);
        lv_country.setAdapter(countryApdater);
    }

    public void initData(){
        countryList = new ArrayList<>();

        countryList.add(new Country(
            "Brazil", R.drawable.flag_br, 5
        ));
        countryList.add(new Country(
            "ES", R.drawable.flag_es, 98
        ));
        countryList.add(new Country(
            "France", R.drawable.flag_fr, 35
        ));
        countryList.add(new Country(
            "Italian", R.drawable.flag_it, 55
        ));
        countryList.add(new Country(
            "Pot", R.drawable.flag_pt, 50
        ));
        countryList.add(new Country(
            "Vietnamese", R.drawable.flag_vn, 115
        ));
    }
}
