package bkap.labguides.viewdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import bkap.labguides.R;

public class EbookDetailActivity extends AppCompatActivity {

    TextView tv_title, tv_category, tv_description;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ebook_detail);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        tv_title = findViewById(R.id.tv_title_ebook);
        tv_category = findViewById(R.id.tv_category);
        tv_description = findViewById(R.id.tv_description);
        imageView = findViewById(R.id.imageview_ebook);

        // nhận dữ liệu tử activity main

        Intent intent = getIntent();
        String title = intent.getExtras().getString("Title");
        String category = intent.getExtras().getString("Category");
        String description = intent.getExtras().getString("Description");
        int image = intent.getExtras().getInt("Thumbnail");


        // gán dữ liệu cho view
        tv_title.setText(title);
        tv_category.setText(category);
        tv_description.setText(description);
        imageView.setImageResource(image);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
