package bkap.labguides.viewdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import bkap.labguides.MainActivity;
import bkap.labguides.R;
import bkap.labguides.helpers.SharedPreferencesHelper;

public class LoginActivity extends AppCompatActivity {

    Button btn_login, btn_cancel;
    EditText et_tendangnhap, et_matkhau;
    CheckBox chk_remember;
    SharedPreferencesHelper sharedPreferencesHelper = new SharedPreferencesHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if(sharedPreferencesHelper.getSharedPreferenceString("username").equals("admin")){
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }

        chk_remember = findViewById(R.id.chk_remember);

        et_tendangnhap = findViewById(R.id.et_tendangnhap);
        et_matkhau = findViewById(R.id.et_matkhau);

        btn_login = findViewById(R.id.btn_login);
        btn_cancel = findViewById(R.id.btn_cancel);




    }

    public void Button_CLick(View view){
        switch (view.getId()){
            case R.id.btn_login:
                if(et_tendangnhap.getText().toString().toLowerCase().equals("admin") && et_matkhau.getText().toString().equals("123")){
                    if(chk_remember.isChecked()){

                        sharedPreferencesHelper.setSharedPreferenceString("username", et_tendangnhap.getText().toString().toLowerCase());
                        sharedPreferencesHelper.setSharedPreferenceString("password", et_matkhau.getText().toString());
                    }
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }else{
                    Toast.makeText(this, "Nhập sai thông tin đăng nhập!", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.btn_cancel:
                Toast.makeText(this, "Button cancel clicked!", Toast.LENGTH_LONG).show();
                break;
        }
    }
}
