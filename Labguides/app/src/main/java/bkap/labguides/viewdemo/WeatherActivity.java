package bkap.labguides.viewdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.GridView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import bkap.labguides.R;
import bkap.labguides.adapters.WeatherAdapter;
import bkap.labguides.models.Weather;

public class WeatherActivity extends AppCompatActivity {

    private ArrayList<Weather> weatherList = new ArrayList<Weather>();
    private GridView mGridView;
    private ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        initData();

        WeatherAdapter weatherAdapter = new WeatherAdapter(WeatherActivity.this, R.layout.item_weather, weatherList);
        mListView = findViewById(R.id.lv_weather);

        mListView.setAdapter(weatherAdapter);

        mGridView = findViewById(R.id.gv_weather);

        mGridView.setAdapter(weatherAdapter);
    }

    private void initData(){
        weatherList.add(new Weather(R.drawable.ic_weather_tuyet,"Berlin", "Snowing", 0));
        weatherList.add(new Weather(R.drawable.ic_weather_mua, "Bangalore", "Rainy",18));
        weatherList.add(new Weather(R.drawable.ic_weather_nang, "London", "Sunny",28));
        weatherList.add(new Weather(R.drawable.ic_weather_nangto, "New York", "Cloudy",33));
        weatherList.add(new Weather(R.drawable.ic_weather_muanho, "Sydney", "Rainy",15));
    }
}
