package bkap.labguides.viewdemo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import bkap.labguides.R;

public class MapsDemoActivity extends AppCompatActivity {
    private static final int REQUEST_LOCATION = 121;
    final String TAG = "MapsDemoActivity";

    LocationManager locationManager;
    Button btn_check_gps, btn_check_network, btn_get_position;
    TextView tv_kinhdo, tv_vido;
    double lat, lng;
    String address, city, state, district, country, postalCode, knownName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_demo);

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        tv_kinhdo = findViewById(R.id.tv_kinhdo);
        tv_vido = findViewById(R.id.tv_vido);

        btn_check_gps = findViewById(R.id.btn_check_gps);
        btn_check_network = findViewById(R.id.btn_check_network);
        btn_get_position = findViewById(R.id.btn_get_position);

        btn_check_network.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkEnableNetwork();
            }
        });

        btn_check_gps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkEnableGPS();
            }
        });

        btn_get_position.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getLocationInfo(LocationManager.GPS_PROVIDER);
                getAddress();
            }
        });
    }

    private void checkEnableGPS() {
        boolean enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        // Nếu GPS chưa được bật thì hiển thị cài đặt GPS
        if (!enabled) {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
        } else {
            Toast.makeText(this, "GPS được bật", Toast.LENGTH_SHORT).show();
            getLocationInfo(LocationManager.GPS_PROVIDER);
        }
    }

    private void checkEnableNetwork(){
        // kiem tra trang thai network
        boolean isGPSEnable = locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        // Hien thi trang thai Network
        if(isGPSEnable){
            Toast.makeText(this, "Network duoc bat", Toast.LENGTH_SHORT).show();
            getLocationInfo(LocationManager.GPS_PROVIDER);
        }else{
            Toast.makeText(this, "Network dang tat", Toast.LENGTH_SHORT).show();
        }
    }

    private void getLocationInfo(String gpsProvider) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION);
//            return;
        }
        try {
            LocationListener locationListener = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                     lat = location.getLatitude();
                     lng = location.getLongitude();
                    tv_kinhdo.setText(String.valueOf(lat));
                    tv_vido.setText(String.valueOf(lng));
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            };

            locationManager.requestLocationUpdates(gpsProvider, 1500, 10, locationListener);
        }catch (SecurityException s){
            Log.d(TAG, "getLocationInfo: " + s.getLocalizedMessage());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        switch (requestCode){
//            case requestPermisstion:
//
//        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void getAddress(){
//        final ProgressDialog progress = ProgressDialog.show(this, "Thông báo",
//                "Đang truy cập thông tin GPS", true);

        new Thread(new Runnable() {
            @Override
            public void run() {
                /*
                 * Hoặc sử dụng API GOOGLE:
                 * http://maps.googleapis.com/maps/api/geocode/json?latlng=21,105&sensor=false&language=en
                 */
                Geocoder gCoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                ArrayList<Address> addresses;
                try {
                    addresses = (ArrayList<Address>) gCoder.getFromLocation(
                            lat, lng, 1);
                    if (addresses != null && addresses.size() > 0) {
                        address = addresses.get(0).getAddressLine(0);
                        city = addresses.get(0).getLocality();
                        state = addresses.get(0).getAdminArea();
                        district = addresses.get(0).getSubAdminArea();
                        country = addresses.get(0).getCountryName();
                        postalCode = addresses.get(0).getPostalCode();
                        knownName = addresses.get(0).getFeatureName();
                        Log.e(TAG, address + "\n" + city + "\n" +
                                state + "\n" + country + "\n" +
                                postalCode + "\n" + knownName + "\n");
                    } else {
                        city = "Không tìm thấy dữ liệu";
                        district = "Không tìm thấy dữ liệu";
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                runOnUiThread(new Runnable() {
                    public void run() {
                        TextView thanhpho = (TextView) findViewById(R.id.tv_city);
                        TextView quan = (TextView) findViewById(R.id.tv_district);
                        thanhpho.setText(address + "\n" + city + "\n" +
                                state + "\n");
                        quan.setText(country + "\n" +
                                postalCode + "\n" + knownName + "\n");
//                        progress.cancel();
                    }
                });
            }
        }).start();
    }
}
