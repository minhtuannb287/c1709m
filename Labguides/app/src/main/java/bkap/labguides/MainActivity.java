package bkap.labguides;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.widget.TableRow;

import bkap.labguides.navigation.MenuDemoActivity;
import bkap.labguides.viewdemo.EbookRecyclerViewActivity;
import bkap.labguides.viewdemo.GridLayoutDemoActivity;
import bkap.labguides.viewdemo.LazadaDemoActivity;
import bkap.labguides.viewdemo.ListViewDemoActivity;
import bkap.labguides.viewdemo.LoginActivity;
import bkap.labguides.viewdemo.MapsDemoActivity;
import bkap.labguides.viewdemo.TableLayoutDemoActivity;
import bkap.labguides.viewdemo.WeatherActivity;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = new Intent(this, MenuDemoActivity.class);
        startActivity(intent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }
}
