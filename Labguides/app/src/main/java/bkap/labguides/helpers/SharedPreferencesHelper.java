package bkap.labguides.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesHelper {

    private Activity mActivity;

    // Tạo contractor để truyền activity

    public SharedPreferencesHelper(Activity mActivity) {
        this.mActivity = mActivity;
    }

    /**
     * Phương thức dùng để lưu trữ dữ liệu vào Preference
     * @param
     * key: Khoá để lưu trữ dữ liệu
     * value: dữ liệu cần lưu trữ
     */
    public void setSharedPreference(String key, boolean value){

        SharedPreferences sharedPreferences = mActivity.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key,value);
        editor.commit();
    }

    public boolean getSharedPreference(String key){
        SharedPreferences sharedPreferences = mActivity.getPreferences(Context.MODE_PRIVATE);
        //getBoolean có 2 tham số 1 là giá trị được lưu 2 là giá trị mặc định nếu null
        boolean value = sharedPreferences.getBoolean(key,false);
        return value;
    }


    public void setSharedPreferenceString(String key, String value){

        SharedPreferences sharedPreferences = mActivity.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key,value);
        editor.commit();
    }

    public String getSharedPreferenceString(String key){
        SharedPreferences sharedPreferences = mActivity.getPreferences(Context.MODE_PRIVATE);
        //getString có 2 tham số 1 là giá trị được lưu 2 là giá trị mặc định nếu null
        String value = sharedPreferences.getString(key,"");
        return value;
    }
}
