package bkap.labguides.navigation;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import bkap.labguides.R;

public class SearchActivity extends AppCompatActivity {

    TextView tv_search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

// Get the intent, verify the action and get the query
        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
//            doMySearch(query);
        }

        // Lấy action bar
        ActionBar actionBar = getSupportActionBar();
        // Bật Back navigation trên Action Bar icon
        actionBar.setDisplayHomeAsUpEnabled(true);
        tv_search = findViewById(R.id.tv_search);
        handleIntent(getIntent());
    }

    @Override
    public boolean onSearchRequested() {
        Toast.makeText(this, "onSearchRequested", Toast.LENGTH_LONG).show();
        return super.onSearchRequested();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    /**a
     * Quản lý dữ liệu intent gửi tới
     */
    private void handleIntent(Intent intent) {
        String a = intent.getAction();
        if (Intent.ACTION_SEARCH.equals(a)) {
            String query = intent.getStringExtra(SearchManager.QUERY);
/**
 * Sử dụng truy vấn hiển thị kết quả tìm kiếm như là:
 * 1. Lấy dữ liệu từ SQLite và hiển thị lên listview
 * 2. Tạo truy vấn tới nguồn internet lấy và hiển thị dữ liệu trả về * Tình huống này chỉ hiển thị text nhập.
 */
            tv_search.setText("Search Query: " + query);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}