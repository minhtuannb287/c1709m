package bkap.labguides.navigation;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import bkap.labguides.R;

/**
 * Tạo và sử dụng Context Menu
 * Tạo và sử dụng Popup Menu
 * Sử dụng Option Menu - ActionBar
 * Xử lý một số ActionBar đặc biệt như SearchView, DropdownList
 * Bật nút HomeAsUp tương tự hành động nút Back nhưng nằm ở Activity
 * Hiển thị tách menu actionbar thêm 1 dòng bên dưới màn hình
 * Tùy chỉnh Dialog cho phù hợp style
 * Sử dụng từ template có menu dựng sẵn
 */


public class MenuDemoActivity extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener{

    String[] mContact = new String[]{
            "Nguyễn Công Phượng", "Mạc Hồng Quân", "Thùy Chi", "Lam Trường", "Đan Trường",
            "Đàm Vĩnh Hưng","Quốc Anh","Xuân Hinh","Quang Lê","Phan Mạnh Quỳnh","Thủy Top",
            "Vàng Anh","Miu Lê","Chipu","Trang Moon","Candy Hà"
    };
    ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_demo);

        ActionBar actionBar = getSupportActionBar();
//        actionBar.setCustomView(R.layout.menu_search);
//
//        // lấy widget edittext
//        EditText edt_search = actionBar.getCustomView().findViewById(R.id.edt_search);
//        edt_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                Toast.makeText(MenuDemoActivity.this, "Text search trigger", Toast.LENGTH_SHORT).show();
//
//                return false;
//            }
//        });
//
////        Cài đặt chế độ hiển thị
//        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME);
//
//        ImageButton btn_search = actionBar.getCustomView().findViewById(R.id.btn_search);
//        btn_search.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(MenuDemoActivity.this, SearchActivity.class);
//
//                startActivity(intent);
//            }
//        });

        // 1. Khởi tạo Adapter
        ArrayAdapter<String> mArrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, mContact);

        // 2. Lấy listview trên layout về
        mListView = findViewById(R.id.listViewContact);

        // 3. Gán adapter lên listview
        mListView.setAdapter(mArrayAdapter);

        // 4. Đăng ký sự kiện long_click đối tượng trên listview thì show ContextMenu ra
        registerForContextMenu(mListView);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        // Gán ContextMenu cho listview
        AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) menuInfo;
        menu.setHeaderTitle("Select The Action For " + mContact[adapterContextMenuInfo.position]);

        //Gán menu_phone đã thiết kế cho sự kiện click item listview
        getMenuInflater().inflate(R.menu.menu_phone, menu);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        AdapterView.AdapterContextMenuInfo adapterContextMenuInfo =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        String name = mContact[adapterContextMenuInfo.position];
        switch (item.getItemId()){
            case R.id.mn_call:
                Toast.makeText(MenuDemoActivity.this, "Call to " + name,
                        Toast.LENGTH_SHORT).show();
                break;
            case R.id.mn_sms:
                Toast.makeText(MenuDemoActivity.this, "SMS to " + name,
                        Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(MenuDemoActivity.this, "Do something with " + name,
                        Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onContextItemSelected(item);
    }

    public void onShowMailBox(View view){
        // Khởi động PopupMenu và neo vào view kích hoạt
        PopupMenu mPopupMenu = new PopupMenu(MenuDemoActivity.this, view);


        // Từ API 11 trở về trước sử dụng code:
        MenuInflater inflater = mPopupMenu.getMenuInflater();
        inflater.inflate(R.menu.menu_mailbox, mPopupMenu.getMenu());
        mPopupMenu.show();
    }

    public void onAddOption(View view){
        // Khởi tạo PopupMenu và neo vào view kích hoạt
        PopupMenu mPopupMenu = new PopupMenu(MenuDemoActivity.this, view);

        // Cài đặt xử lý sự kiện
        mPopupMenu.setOnMenuItemClickListener(this);

        // Từ API 14 trở về sau
        mPopupMenu.inflate(R.menu.menu_addlist);
        mPopupMenu.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()){
            case R.id.mn_add_mail:
                Toast.makeText(MenuDemoActivity.this, "Them Email", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.mn_add_docx:
                Toast.makeText(MenuDemoActivity.this, "Them Docx", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.mn_add_file:
                Toast.makeText(MenuDemoActivity.this, "Them Tai lieu", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.mn_open_game:
                Toast.makeText(MenuDemoActivity.this, "New Game", Toast.LENGTH_SHORT).show();
                return true;
// Bắt sự kiện tương tự cho các menu khác ...
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}