package bkap.labguides.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import bkap.labguides.R;
import bkap.labguides.models.Book;
import bkap.labguides.viewdemo.EbookDetailActivity;

public class EbookAdapter extends RecyclerView.Adapter<EbookAdapter.MyViewHolder> {

    private Context mContext;
    private List<Book> mData;

    public EbookAdapter(Context mContext, List<Book> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.item_cardview_ebook, parent, false);


        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final Book mBook = mData.get(position);
        holder.tv_title.setText(mBook.getTitle());
        holder.imageView.setImageResource(mBook.getImage());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, EbookDetailActivity.class);

                //Chuyển dữ liệu qua book activity
                intent.putExtra("Title", mBook.getTitle());
                intent.putExtra("Category", mBook.getCategory());
                intent.putExtra("Description", mBook.getDescription());
                intent.putExtra("Thumbnail", mBook.getImage());

                // Khoi chay book activity
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView tv_title;
        ImageView imageView;
        CardView cardView;

        public MyViewHolder(View itemView){
            super(itemView);

            tv_title = itemView.findViewById(R.id.tv_title_ebook);
            imageView = itemView.findViewById(R.id.imageview_ebook);
            cardView = itemView.findViewById(R.id.carview_ebook);
        }


    }
}
