package bkap.labguides.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.w3c.dom.Text;

import java.util.List;

import bkap.labguides.R;
import bkap.labguides.models.Country;

public class CountryApdater extends ArrayAdapter<Country> {

    private Context mContext;
    private int resID;
    List<Country> countryList;

    public CountryApdater(@NonNull Context context, int resource, @NonNull List<Country> objects) {
        super(context, resource, objects);

        this.mContext = context;
        this.resID = resource;
        this.countryList = objects;


    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view =convertView;
        Country mCountry = countryList.get(position);
        ViewHolder viewHolder = new ViewHolder();
        if(view == null){
            view = LayoutInflater.from(mContext).inflate(resID,null);
            viewHolder.imageView = view.findViewById(R.id.iv_country);
            viewHolder.tv_country = view.findViewById(R.id.tv_country);
            viewHolder.tv_rank = view.findViewById(R.id.tv_rank);

            view.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder)view.getTag();
        }
        viewHolder.imageView.setImageResource(mCountry.getFlag());
        viewHolder.tv_country.setText(mCountry.getCountry());
        viewHolder.tv_rank.setText((mCountry.getRank() +""));
        return view;
    }

    private class  ViewHolder{
        ImageView imageView;
        TextView tv_country;
        TextView tv_rank;
    }
}
