package bkap.labguides.adapters;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import bkap.labguides.R;
import bkap.labguides.models.Weather;

public class WeatherAdapter extends ArrayAdapter<Weather> {

    private Context context;
    private ArrayList<Weather> weatherList;
    private int itemLaytout;
    private LayoutInflater layoutInflater;

    public WeatherAdapter(Context context, int resource, ArrayList<Weather> objects) {
        super(context, resource, objects);
        this.context = context;
        this.itemLaytout = resource;
        this.weatherList = objects;
        this.layoutInflater = (LayoutInflater) context .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView==null){
            convertView = layoutInflater.inflate(itemLaytout, null);
        }

        ImageView iv_image;
        TextView tv_country;
        TextView tv_description;
        TextView tv_celsius;

        iv_image = convertView.findViewById(R.id.iv_image);
        tv_country = convertView.findViewById(R.id.tv_country);
        tv_description = convertView.findViewById(R.id.tv_description);
        tv_celsius = convertView.findViewById(R.id.tv_celsius);

        final Weather weather = weatherList.get(position);

        Log.e("WeatherAdapter", String.valueOf(weather.getImage()));
        iv_image.setImageResource(weather.getImage());
        tv_country.setText(weather.getCountry());
        tv_description.setText(weather.getDescription());
        tv_celsius.setText(weather.getCelsius()+"\u2103");

        return convertView;
    }

}
