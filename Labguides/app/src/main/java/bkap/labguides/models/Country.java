package bkap.labguides.models;

public class Country {
    private String country;
    private int flag;
    private int rank;

    public Country() {
    }

    public Country(String country, int flag, int rank) {
        this.country = country;
        this.flag = flag;
        this.rank = rank;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }
}
