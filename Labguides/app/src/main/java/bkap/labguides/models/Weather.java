package bkap.labguides.models;

public class Weather  {
    private int image;
    private String country;
    private String description;
    private int celsius;

    public Weather() {
    }

    public Weather(int image, String country, String description, int celsius) {
        this.image = image;
        this.country = country;
        this.description = description;
        this.celsius = celsius;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCelsius() {
        return celsius;
    }

    public void setCelsius(int celsius) {
        this.celsius = celsius;
    }
}
