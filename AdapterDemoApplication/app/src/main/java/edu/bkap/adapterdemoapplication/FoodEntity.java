package edu.bkap.adapterdemoapplication;

public class FoodEntity {
    private String name;
    private int thumbnail;
    private boolean status;
    private String description;

    public FoodEntity() {
    }

    public FoodEntity(String name, String description, int thumbnail, boolean status) {
        this.name = name;
        this.thumbnail = thumbnail;
        this.status = status;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
