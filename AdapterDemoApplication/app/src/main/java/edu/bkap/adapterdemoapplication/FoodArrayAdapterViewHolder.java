package edu.bkap.adapterdemoapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public class FoodArrayAdapterViewHolder extends ArrayAdapter<FoodEntity> {
    private Context mContext;
    private int layoutItem;
    private List<FoodEntity> foodEntitiesList;

    public FoodArrayAdapterViewHolder(@NonNull Context context, int resource, @NonNull List<FoodEntity> objects) {
        super(context, resource, objects);

        this.mContext = context;
        this.layoutItem = resource;
        this.foodEntitiesList = objects;
    }

    public void getSelectedItems(){
//        List<FoodEntity> mList = new ArrayList<>();
        StringBuilder stringBuilder = new StringBuilder();
        for(FoodEntity foodEntity : foodEntitiesList){
            if(foodEntity.isStatus()){
//                mList.add(foodEntity);
               stringBuilder.append(foodEntity.getName()+", ");
            }
        }
        Toast.makeText(mContext, stringBuilder.toString(), Toast.LENGTH_SHORT).show();
//        return mList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        final FoodEntity foodEntity = foodEntitiesList.get(position);
        ViewHolder viewHolder = new ViewHolder();
        if(view == null){
            view = LayoutInflater.from(mContext).inflate(layoutItem, parent, false);
            viewHolder.tv_name = view.findViewById(R.id.tv_name);
            viewHolder.tv_description = view.findViewById(R.id.tv_description);
            viewHolder.imageView = view.findViewById(R.id.imageView);
            viewHolder.checkBox = view.findViewById(R.id.cb_status);
            view.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                foodEntity.setStatus(((CheckBox) v).isChecked());
            }
        });

        viewHolder.imageView.setImageResource(foodEntity.getThumbnail());
        viewHolder.tv_name.setText(foodEntity.getName());
        viewHolder.tv_description.setText(foodEntity.getDescription());
        viewHolder.checkBox.setChecked(foodEntity.isStatus());

        return view;
    }

    private class ViewHolder {
        TextView tv_name, tv_description;
        ImageView imageView;
        CheckBox checkBox;
    }
}
