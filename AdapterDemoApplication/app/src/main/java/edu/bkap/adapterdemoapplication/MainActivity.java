package edu.bkap.adapterdemoapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private final String TAG = "MainActivity";
    List<FoodEntity> mList ;
    FoodArrayApdater foodArrayApdater;
    FoodArrayAdapterViewHolder foodArrayAdapterViewHolder;
    ListView mListView;

    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initData();

//        initAdapter();
        initAdapterViewHolder();


        btn = findViewById(R.id.btnChon);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: ");
                foodArrayAdapterViewHolder.getSelectedItems();
            }
        });

        mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "onItemLongClick: ");
                Toast.makeText(MainActivity.this, mList.get(position).getName().toString(), Toast.LENGTH_SHORT).show();

                return false;
            }
        });

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "onItemClick: ");
                Toast.makeText(MainActivity.this, "Item click" + mList.get(position).getName().toString(), Toast.LENGTH_SHORT).show();
            }
        });


    }

    public void initData(){
        mList = new ArrayList<>();
        mListView = findViewById(R.id.mListView);

//        mListView.setItemsCanFocus(true);

        mList.add(new FoodEntity("F1", "Mon 01", R.drawable.food1, false));
        mList.add(new FoodEntity("F2", "Mon 02", R.drawable.food2, false));
        mList.add(new FoodEntity("F3", "Mon 03", R.drawable.food3, false));
        mList.add(new FoodEntity("F4", "Mon 04", R.drawable.food4, false));
        mList.add(new FoodEntity("F5", "Mon 05", R.drawable.food5, true));
        mList.add(new FoodEntity("F6", "Mon 06", R.drawable.food6, true));
        mList.add(new FoodEntity("F1", "Mon 01", R.drawable.food1, false));
        mList.add(new FoodEntity("F2", "Mon 02", R.drawable.food2, false));
        mList.add(new FoodEntity("F3", "Mon 03", R.drawable.food3, false));
        mList.add(new FoodEntity("F4", "Mon 04", R.drawable.food4, false));
        mList.add(new FoodEntity("F5", "Mon 05", R.drawable.food5, true));
        mList.add(new FoodEntity("F6", "Mon 06", R.drawable.food6, true));
    }

    public void initAdapter(){
        foodArrayApdater = new FoodArrayApdater(this, R.layout.item_food, mList);
        mListView.setAdapter(foodArrayApdater);

    }

    public void initAdapterViewHolder(){
        foodArrayAdapterViewHolder = new FoodArrayAdapterViewHolder(this, R.layout.item_food, mList);
        mListView.setAdapter(foodArrayAdapterViewHolder);
    }
}
