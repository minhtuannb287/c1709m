package edu.bkap.adapterdemoapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

public class FoodArrayApdater extends ArrayAdapter<FoodEntity> {

    private Context mContext;
    private int layoutItem;
    private List<FoodEntity> foodEntities;

    public FoodArrayApdater(@NonNull Context context, int resource, @NonNull List<FoodEntity> objects) {
        super(context, resource, objects);

        this.mContext = context;
        layoutItem = resource;
        foodEntities = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        FoodEntity foodEntity = foodEntities.get(position);

        convertView = LayoutInflater.from(mContext).inflate(layoutItem, parent, false);

        TextView tv_name,tv_description;
        ImageView imageView;
        CheckBox checkBox;

        tv_name=convertView.findViewById(R.id.tv_name);
        tv_description=convertView.findViewById(R.id.tv_description);
        imageView = convertView.findViewById(R.id.imageView);
        checkBox = convertView.findViewById(R.id.cb_status);

        tv_name.setText(foodEntity.getName());
        tv_description.setText(foodEntity.getDescription());
        imageView.setImageResource(foodEntity.getThumbnail());
        checkBox.setChecked(foodEntity.isStatus());

        return convertView;

    }



}
