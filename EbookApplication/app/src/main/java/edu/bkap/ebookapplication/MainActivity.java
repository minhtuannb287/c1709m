package edu.bkap.ebookapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import io.realm.Realm;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity {

    final String TAG = "MainACTIVITY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Realm.init(this);

        final Realm realm = Realm.getDefaultInstance();


        final CategoryEntity categoryEntity = new CategoryEntity();
        categoryEntity.setName("Kiem hiep");
        categoryEntity.setState(true);

        // Cach 1
       /* realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                // This will create a new object in Realm or throw an exception if the
                // object already exists (same primary key)
                // realm.copyToRealm(obj);

                // This will update an existing object with the same primary key
                // or create a new object if an object with no primary key = 42
                realm.copyToRealmOrUpdate(categoryEntity);
            }
        });*/

       //Cach 2

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm eRealm) {
                Number currentIdNum = eRealm.where(CategoryEntity.class).max("id");
                int nextId;
                if(currentIdNum == null) {
                    nextId = 1;
                } else {
                    nextId = currentIdNum.intValue() + 1;
                }
                CategoryEntity categoryEntity1 = new CategoryEntity();
                categoryEntity1.setId(nextId);
                categoryEntity1.setName("Truyen cuoi");
                categoryEntity1.setState(false);
                eRealm.insertOrUpdate(categoryEntity1);
            }
        });

        final RealmResults<CategoryEntity> categoryEntities = realm.where(CategoryEntity.class).findAll();
        Log.d(TAG, "onCreate: " + categoryEntities.size());
//        realm.beginTransaction();

    }
}
