package edu.bkap.usermanagement;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class UserDAO {
    MySQLiteOpenHelper mySQLiteOpenHelper;
    SQLiteDatabase mSQLiteDatabase;

    public UserDAO(Context mContext) {
        this.mySQLiteOpenHelper = new MySQLiteOpenHelper(mContext);
        mSQLiteDatabase = mySQLiteOpenHelper.getWritableDatabase();
    }

    public void closeConnection(){
        if(mSQLiteDatabase != null){
            mSQLiteDatabase.close();
        }
    }

    public ArrayList<UserEntity> getAll(){
        String sql = "SELECT * FROM user";
        UserEntity userEntity;
        ArrayList<UserEntity> mArrayList = new ArrayList<>();
        Cursor cursor = mSQLiteDatabase.rawQuery(sql, null);
        while (cursor.moveToNext()){
            userEntity = new UserEntity();
            userEntity.setId(cursor.getInt(0));
            userEntity.setUsername(cursor.getString(1));
            userEntity.setEmail(cursor.getString(2));
            userEntity.setPassword(cursor.getString(3));
            mArrayList.add(userEntity);
        }

        return mArrayList;
    }

    public void Create(UserEntity obj){
        String sql = "INSERT INTO user(username, email, password) VALUES('"
                + obj.getUsername() + "','"
                + obj.getEmail() + "','"
                + obj.getPassword() + "')";
        mSQLiteDatabase.execSQL(sql);
    }

    public void Update(UserEntity obj){
        String sql = "UPDATE user SET username='" + obj.getUsername() + "',"
                + "email ='" + obj.getEmail() + "', "
                + "password='" + obj.getPassword() + "' "
                + "WHERE id=" + obj.getId();
        mSQLiteDatabase.execSQL(sql);
    }

    public void Delete(int id){
        String sql = "DELETE FROM user WHERE id = " + id;
        mSQLiteDatabase.execSQL(sql);

    }
}
