package edu.bkap.usermanagement;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MySQLiteOpenHelper extends SQLiteOpenHelper {
    private static final String DB_NAME="bkap_user.db";
    private static final int DB_VERSION=3;


    public MySQLiteOpenHelper(Context mContext){
        super(mContext, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String sql = "CREATE TABLE user(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "username VARCHAR(50) NOT NULL,"
                + "email VARCHAR(50) NOT NULL, "
                + "password VARCHAR(20) NOT NULL"
                + ")";
        db.execSQL(sql);

        sql = "INSERT INTO user(username, email, password) VALUES('admin', 'admin@bkap.edu', '123')";
        db.execSQL(sql);
        sql = "INSERT INTO user(username, email, password) VALUES('admin2', 'admin2@bkap.edu', '123')";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        if(oldVersion != newVersion){
            String sql = "DROP TABLE IF EXISTS user";
            db.execSQL(sql);
            onCreate(db);
        }
    }
}
