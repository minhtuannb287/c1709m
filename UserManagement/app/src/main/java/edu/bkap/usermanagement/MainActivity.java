package edu.bkap.usermanagement;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public static final int REQUEST_CODE_ADD = 110, REQUEST_CODE_EDIT=111,
            RESULT_CODE_CANCEL=100,
            RESULT_CODE_SAVE=101;
    ListView mListView;
    ArrayList<UserEntity> mArrayList;
    ArrayAdapter<UserEntity> mArrayAdapter;
    UserDAO userDAO ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        mListView = findViewById(R.id.mListView);
        registerForContextMenu(mListView);

        loadData();
    }

    public void loadData(){
        userDAO = new UserDAO(this);
        mArrayList = userDAO.getAll();
        mArrayAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1, mArrayList);
        mListView.setAdapter(mArrayAdapter);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Cập nhật User");

        getMenuInflater().inflate(R.menu.menu_context, menu);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int position = info.position;
        UserEntity userEntity = mArrayList.get(position);
        switch (item.getItemId()){
            case R.id.mn_edit:


                Intent intent = new Intent(MainActivity.this, UserFormActivity.class);

                Bundle bundle = new Bundle();
                bundle.putSerializable("user", userEntity);
                intent.putExtras(bundle);

                startActivityForResult(intent,REQUEST_CODE_EDIT);

                break;
            case R.id.mn_delete:
                userDAO.Delete(userEntity.getId());
                loadData();
//                mArrayAdapter.notifyDataSetChanged();
                break;

        }
        return super.onContextItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_actionbar, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case R.id.mn_create:
                Intent intent = new Intent(MainActivity.this, UserFormActivity.class);

                startActivityForResult(intent, REQUEST_CODE_ADD);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        intent.putExtra("requestCode",requestCode);
        super.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (resultCode){
            case RESULT_CODE_SAVE:
                UserEntity userEntity = (UserEntity) data.getSerializableExtra("userEntity");
                if(requestCode== REQUEST_CODE_ADD){
                    userDAO.Create(userEntity);
                    loadData();
                    Toast.makeText(MainActivity.this,"Thêm thành công",Toast.LENGTH_SHORT).show();
                }else{
                    userDAO.Update(userEntity);
                    loadData();
                    Toast.makeText(MainActivity.this,"Sửa thành công",Toast.LENGTH_SHORT).show();
                }
                mArrayAdapter.notifyDataSetChanged();

                break;
            case RESULT_CODE_CANCEL:
                Toast.makeText(MainActivity.this, "Cancel", Toast.LENGTH_SHORT).show();
                break;
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        userDAO.closeConnection();
    }
}
