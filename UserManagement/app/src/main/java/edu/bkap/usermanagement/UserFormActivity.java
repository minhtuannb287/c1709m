package edu.bkap.usermanagement;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.service.autofill.UserData;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class UserFormActivity extends AppCompatActivity implements View.OnClickListener{

    EditText edt_username, edt_email, edt_password;
    Button btn_luu, btn_boqua;
    UserEntity userEntity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_form);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        init();

        Bundle bundle = getIntent().getExtras();
        int type = getIntent().getExtras().getInt("requestCode",MainActivity.REQUEST_CODE_ADD);

        if(type == MainActivity.REQUEST_CODE_ADD){
            userEntity = new UserEntity();
        }else{
            userEntity = (UserEntity) bundle.getSerializable("user");
            edt_username.setText(userEntity.getUsername());
            edt_email.setText(userEntity.getEmail());
            edt_password.setText(userEntity.getPassword());

        }
    }

    public void init(){
        edt_username = findViewById(R.id.edt_username);
        edt_password = findViewById(R.id.edt_password);
        edt_email = findViewById(R.id.edt_email);

        btn_luu = findViewById(R.id.btn_luu);
        btn_boqua = findViewById(R.id.btn_boqua);
        btn_luu.setOnClickListener(this);
        btn_boqua.setOnClickListener(this);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btn_luu:

                userEntity.setUsername(edt_username.getText().toString());
                userEntity.setEmail(edt_email.getText().toString());
                userEntity.setPassword(edt_password.getText().toString());
                Intent intent = new Intent();
                intent.putExtra("userEntity", userEntity);
                setResult(MainActivity.RESULT_CODE_SAVE,intent);

                break;
            case R.id.btn_boqua:

                setResult(MainActivity.RESULT_CODE_CANCEL);

                break;
        }

        finish();
    }
}
